Vue.createApp({
    async mounted() {
      this.reloadTodos();
    },
    template: `
      <h1>TODO APP</h1>
      <input id="inp" placeholder="Nuovo todo" type="text" v-model="newTodoDescription" />

      <button @click="addTodo()">Aggiungi</button>
      <br><br>
      <input id="newDesc" type="text" placeholder="Nuova descrizione" v-model="updateTodoDescription" />
      <svg  v-if="!loaded" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style=" background: rgb(255, 255, 255); display: block; shape-rendering: auto; margin-top: 50px; margin-left: 70px;" width="60px" height="60px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        <path d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#e15b64" stroke="none">
          <animateTransform attributeName="transform" type="rotate" dur="1s" repeatCount="indefinite" keyTimes="0;1" values="0 50 51;360 50 51"></animateTransform>
        </path>
      </svg>

      <ul id="ul">
        <li v-for="todo in todos"><button @click="deleteTodos(todo.id)">X</button> {{ todo.description }} <input v-bind:id="ind(todo.id)" type="checkbox" v-bind:checked="done(todo.done)"><button @click="update(todo.id)">Aggiorna</button></li>
      </ul>
    `,
    data() {
      return {
        newTodoDescription: '',
        todos: [],
        loaded: false,
          updateTodoDescription: '',
      }
    },
    methods: {
      async addTodo() {
        this.loaded = false;
        ul = document.getElementById("ul");
        ul.hidden = true;
        await fetch('http://localhost:8080/api/todos', {
            method: 'POST',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({description: this.newTodoDescription})
        });
        this.reloadTodos();
      },
      async reloadTodos() {
        var todosJSON = await (await fetch('http://localhost:8080/api/todos')).json();
        this.todos = todosJSON;
        this.loaded = true;
        ul = document.getElementById("ul");
        ul.hidden = false;

      },
      async deleteTodos(id) {
        this.loaded = false;
        ul = document.getElementById("ul");
        ul.hidden = true;
        await fetch('http://localhost:8080/api/todos/' + id, {
            method: 'DELETE',
            headers: {'Content-type' : 'application/json'}
        }, );
        this.reloadTodos();
      },
        done(checked) {
            if (checked === 0) {
                return false
            } else {
                return true;
            }
        },
        async update(id) {
            ch = document.getElementById(id+"").checked;
            this.loaded = false;
            ul = document.getElementById("ul");
            ul.hidden = true;
            await fetch('http://localhost:8080/api/todos/' + id, {
                method: 'PUT',
                headers: {'Content-type' : 'application/json'},
                body: JSON.stringify({description: this.updateTodoDescription, done: ch})
            });
            this.reloadTodos();
        },
        ind(id) {
          return id + "";
        }

    }
  }).mount('#app');
