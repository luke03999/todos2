<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;


class TodoController extends Controller
{


    public function add(Request $request)
    {
        $description = $request->input('description');

        $results = app('db')->insert("INSERT INTO tasks(description, done, insertDate) VALUES('$description', false, now())");

        // Return 201 created
        return new Response(null, 201);
    }

    public function delete(Request $request, $id)
    {

        $results = app('db')->insert("DELETE FROM tasks WHERE id = $id");

        // Ritorno 204 no content
        return new Response(null, 204);
    }

    public function update(Request $request, $id)
    {
        $description = $request->input('description');
        $done = $request->input('done');

        $results = app('db')->insert("UPDATE tasks SET
        description = '$description',
        done = $done
        WHERE id = $id");

        // Ritorno 200 OK
        return new Response(null, 200);
    }


    public function get(Request $request, $id) {


        $results = app('db')->select("SELECT * FROM tasks WHERE id = $id");

        if (count($results) == 0) {
            return new Response(null, 404);
        } else {
            return $results;
        }
    }



}
