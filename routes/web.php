<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/api/info', function () use ($router) {
    return "v 0.0.1";
});



// Chiamata in get
$router->get('/api/todos', function () use ($router) {
    $results = app('db')->select("SELECT * FROM tasks");
    count($results);
    return $results;
});

// Chiamata in post
$router->post('/api/todos', 'TodoController@add'); 

// Chiamata in post
$router->delete('/api/todos/{id}', 'TodoController@delete'); 


// Chiamata in put
$router->put('/api/todos/{id}', 'TodoController@update'); 



















// Chiamata in get per id
$router->get('/api/todos/{id}', 'TodoController@get'); 




